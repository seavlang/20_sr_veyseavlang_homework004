import React, { Component } from "react";
import Swal from "sweetalert2";

export default class Table extends Component {
  onClickDetail = (item) => {
    Swal.fire({
      title: "Details",
      html:
        '<div style="text-align:left; margin-left:100px;">' +
        '<b>ID <span style="padding-left : 80px">: </span>'+ item.id + '</b> </br>'+
        '<b>Email <span style="padding-left : 53px">: </span>  ' +
        `${item.userEmail}` +
        "</b> </br>" +
        '<b>Name <span style="padding-left : 50px">: </span>' +
        `${item.userName}` +
        "</b> </br>" +
        '<b>Age<span style="padding-left : 70px">: </span>' +
        `${item.userAge}` +
        "</b> </br>" +
        '<b>Status<span style="padding-left : 51px">: </span>' +
        item.status +
        "</b> </div>",
      confirmButtonText: '<i class="fa fa-thumbs-up"></i> Ok!',
    });
  };

  render() {
    return (
      <div>
        <div class="w-[900px] mx-auto mt-10">
          <div class="flex flex-col">
            <div class="overflow-x-auto shadow-md sm:rounded-lg">
              <div class="inline-block min-w-full align-middle">
                <div class="overflow-hidden ">
                  <table class="min-w-full divide-y divide-gray-200 table-fixed dark:divide-gray-700">
                    <thead class="bg-gray-100 dark:bg-gray-700">
                      <tr>
                        <th
                          scope="col"
                          class="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400"
                        >
                          ID
                        </th>
                        <th
                          scope="col"
                          class="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400"
                        >
                          Email
                        </th>
                        <th
                          scope="col"
                          class="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400"
                        >
                          UserName
                        </th>
                        <th
                          scope="col"
                          class="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400"
                        >
                          Age
                        </th>
                        <th
                          scope="col"
                          class="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400"
                        >
                          Action
                        </th>
                      </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200 dark:bg-gray-800  dark:divide-gray-700">
                      {this.props.data.map((item) => (
                        <tr class="even:bg-gray-700 hover:bg-gray-600">
                          <td class="py-4 px-6 text-sm font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {item.id}
                          </td>
                          <td class="py-4 px-6 text-sm font-medium text-gray-500 whitespace-nowrap dark:text-white">
                            {item.userEmail}
                          </td>
                          <td class="py-4 px-6 text-sm font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {item.userName}
                          </td>
                          <td class="py-4 px-6 text-sm font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {item.userAge}
                          </td>
                          <td class="py-4 px-6 text-sm font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            <button
                              onClick={() => this.props.onButtonChange(item.id)}
                              type="button"
                              data-te-ripple-init
                              data-te-ripple-color="light"
                              class={`inline-block rounded   hover:bg-gray-500 px-6 pt-2.5 pb-2 text-xs font-medium uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] ${
                                item.status == "Pending"
                                  ? "bg-red-600"
                                  : "bg-green-600"
                              }`}
                            >
                              {item.status}
                            </button>
                            <button
                              type="button"
                              data-te-ripple-init
                              data-te-ripple-color="light"
                              class="ml-5 inline-block rounded bg-blue-700 hover:bg-gray-500 px-6 pt-2.5 pb-2 text-xs font-medium uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)]"
                              onClick={() => this.onClickDetail(item)}
                            >
                              Show More
                            </button>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
