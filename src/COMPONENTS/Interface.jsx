import React, { Component } from "react";
import Table from "./Table";
import Swal from "sweetalert2";

export default class Interface extends Component {
  constructor() {
    super();
    this.state = {
      userInfor: [
        // {
        // id: 1,
        // userEmail: "sievlangvey@gmail.com",
        // userName: "Vey Seavlang",
        // userAge: "19",
        // status: "Pending",
        // },
      ],
      isUserNameValid: false,
      isUserAgeValid:false,
      isUserEmailValid:false,
      userAgeError: "",
      userNameError: "",
      userEmailError: "",
      newEmail: "",
      newName: "",
      newAge: "",
      status: "Pending",
    };
  }

  onChangeHandlerE = (e) => {
    this.setState({
      newEmail: e.target.value,
    });
    console.log(e.target.value);
  };

  onChangeHandlerU = (e) => {
    this.setState({
      newName: e.target.value,
    });
  };

  onChangeHandlerA = (e) => {
    this.setState({
      newAge: e.target.value,
    });
  };

  onSubmit = (e) => {
    e.preventDefault();

    const newObj = {
      id: this.state.userInfor.length + 1,
      userEmail: this.state.newEmail,
      userName: this.state.newName,
      userAge: this.state.newAge,
      status: this.state.status,
    };

    var isUserNameValid = true;
    var isUserAgeValid = true;
    var isUserEmailValid = true;
    var userNameError = "";
    var userAgeError = "";
    var userEmailError = "";
    let pattName = /[^a-z]/gi;
    let pattAge = /[0-9]{3}/gi;
    let pattEmail  = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/gi;

    var userName = this.state.newName;
    var userAge = this.state.newAge;
    var userEmail = this.state.newEmail;

    if (userName === "" ) {
      userNameError = "Username can't be empty.";
      isUserNameValid = false;
    } else if (newObj.userName.match(pattName) != null) {
      userNameError = "Invalid Username";
      isUserNameValid = false;
    } else {
      userNameError = "";
      isUserNameValid = true;
    }

    if (userAge === "" ) {
      userAgeError = "User's Age can't be empty.";
      isUserAgeValid = false;
    } else if (newObj.userAge.match(pattAge) != null) {
      userAgeError = "Invalid Age";
      isUserAgeValid = false;
    } else {
      userAgeError = "";
      isUserAgeValid = true;
    }
    this.setState({
      userNameError,
      userAgeError,
      userEmailError
    })

    if (userEmail === "" ) {
      userEmailError = "User's Email can't be empty.";
      isUserEmailValid = false;
    } else if (!(newObj.userEmail.match(pattEmail) != null)) {
      userEmailError = "Invalid Email";
      isUserEmailValid = false;
    } else {
      userEmailError = "";
      isUserEmailValid = true;
    }
    this.setState({
      userNameError,
      userAgeError,
      userEmailError
    })

    if (isUserNameValid && isUserAgeValid && isUserEmailValid) {
      this.setState({
        userInfor: [...this.state.userInfor, newObj],
        userNameError: "",
        userAgeError: "",
        userEmailError: "",
        newEmail: "",
        newName: "",
        newAge: "",
      });
    }
  };

  onButtonChange = (id) => {
    this.state.userInfor.map((item) =>
      item.id == id
        ? (item.status = item.status === "Pending" ? "Done" : "Pending")
        : item.id
    );

    this.setState({
      userInfor: this.state.userInfor,
      
    });
  };

  render() {
    return (
      <div className="m-auto w-[1000px] h-auto bg-slate-300 text-left p-10 mt-5 rounded-lg ">
        <h1 className="mb-5 ml-[28%] font-font1 text-3xl font-bold bg-gradient-to-r from-indigo-500 via-purple-500 to-pink-500 text-transparent bg-clip-text inline">
          Please Fill Your Information
        </h1>
        <div>
          <label
            for="input-group-1"
            class="block mb-2 text-sm font-medium text-gray-900 "
          >
            Your Email : <p className="text-red-500 inline">{this.state.userEmailError}</p>
          </label>
          <div class="relative mb-5">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
              <svg
                aria-hidden="true"
                class="w-5 h-5 text-gray-500 dark:text-gray-400"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path>
                <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path>
              </svg>
            </div>
            <input
              onChange={this.onChangeHandlerE}
              type="text"
              id="input-group-1"
              class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="name@gmail.com"
              value={this.state.newEmail}
            />
          </div>

          <label
            for="website-admin"
            class="block mb-2 text-sm font-medium text-gray-900"
          >
            Username : <p className="text-red-500 inline">{this.state.userNameError}</p>
          </label>
          <div class="flex mb-5">
            <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
              @
            </span>
            <input
              onChange={this.onChangeHandlerU}
              type="text"
              id="website-admin"
              class="rounded-none rounded-r-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="veyseavlang"
              value={this.state.newName}
            />
          </div>

          <label
            for="website-admin"
            class="block mb-2 text-sm font-medium text-gray-900"
          >
            Age : <p className="text-red-500 inline">{this.state.userAgeError}</p>
          </label>
          <div class="flex">
            <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
              ❤
            </span>
            <input
              onChange={this.onChangeHandlerA}
              type="number"
              id="website-admin"
              class="rounded-none rounded-r-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="19"
              value={this.state.newAge}
            />
          </div>
          <div class="flex justify-center space-x-2 mt-10">
            <button
              onClick={this.onSubmit}
              type="button"
              data-te-ripple-init
              data-te-ripple-color="light"
              class="inline-block rounded bg-gray-700 hover:bg-gray-500 px-6 pt-2.5 pb-2 text-xs font-medium uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)]"
            >
              Register
            </button>
          </div>
        </div>
        <Table
          data={this.state.userInfor}
          onButtonChange={this.onButtonChange}
        />
      </div>
    );
  }
}
